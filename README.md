## Getting Started

This is an example of how you would call our GOP-API services with Python > 3.6.
In this sample, we've included a executable script & sample audio file. It
will print out the results to standard out on your console. 

### Prerequisites

It is assumed you are running Python > 3.6. However, with future-fstrings should execute correctly on Python 2.7 and above.

If you are not, please install future-fstrings module to support fstrings syntax. You'll also need the requests module to call our endpoint


```
python pip install future-fstrings
python pip install requests
```


The sample script was executed on a fresh Python 3.7 instance managed by Anaconda (https://www.anaconda.com/distribution/). 
It's recommended to execute python scripts in an isolated environment to minimise errors. 

### Installation

1. Clone the repository
2. Edit the sample.py script and enter in your "x-api-key" to the variable "ACCESS_KEY". It'll be a string
3. Execute the sample.py script on bash with```
python sample.py
```