# -*- coding: future_fstrings -*-

# this sample script has been tested with Python 3.7
# If you are running python<3.6, please install future-fstrings

# pip install future-fstrings
# https://pypi.org/project/future-fstrings/

# it is required to install the requests module
# python -m pip install requests (https://2.python-requests.org/en/master/)
import base64
import requests 

# audio_format, supports either wav or mp3
audio_format = 'wav'

# file name you wish to send
audio_file = f'AFTER_THE_SHOW_EXAMPLE.{audio_format}'

# spoken content
content = 'After the show we went to a restaurant'

# your access key, should be stored on a secure server & not client side
# insert your access key here for the sample
ACCESS_KEY = '5c931a99cf329e000108738d7e7cb1e4efda4815a01f972cd3478596'

# reads in file via relative paths
audio = open(audio_file, 'rb')
audio_content = audio.read()
base64_audio = base64.b64encode(audio_content).decode()
req_body = {
    "format": audio_format,
    "content": content,
    "audioBase64": base64_audio
}
headers = {
	'x-api-key': ACCESS_KEY,
    'Content-Type': "application/json",
}

r = requests.post(
    f'https://api.languageconfidence.com/pronunciation-trial/score',
    json=req_body,
    headers=headers
)

print(r.json())
